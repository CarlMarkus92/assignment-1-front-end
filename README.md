# Assignment 1 Front-End
Assignment from Noroff to build a dynamic webpage using JavaScript, CSS and HTML 

## Description
The webpage is a work/bank simulation where you work for money that you can transfer to your bank. You can also take up loans. Your money can be used to buy computers. 

## Installation
Files can be downloaded and you can run the webpage by copying the filepath of index.html into your browser searchbar. You can also use a tool such as Live Server in VS - code. 

## Usage
By pressing the work button you earn 100 NOK. 
You can transfer your income to the bank and use the balance to buy computers. The amount you may loan depends on your bank balance. You may only loan a maximum of the double the amount of your balance and can only have one loan at the time. If you have a loan, 10% of your transfers to the bank will go to paying the loan. 
