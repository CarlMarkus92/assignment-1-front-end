//Variables to handle our income(money), balance(bank) and outstanding loan (loan)
let discont = 10;
let money = 0;
let bank = 0;
let loan = 0;
let bonus = 0; 

//The pay loan button is hidden as default
document.getElementById("payLoanBtn").style.visibility = "hidden";

//Function for working. Each click grants 100 money
workFunction = () => {
  money += 100;
  document.getElementById("workPay").innerHTML = money;
  console.log(money);
};

//Function to transfer money to the bank
bankFunction = () => {
  if (money > 0) {
    bank = money + bank;
    //If we have a loan, 10% of income goes towards the loan
    if (loan != 0) {
      let tenPercent = (money * 10) / 100;
      document.getElementById("workBank").innerHTML = bank -= tenPercent;
      document.getElementById("loan").innerHTML = loan -= tenPercent;
      money = 0;
      document.getElementById("workPay").innerHTML = money;
      //if the loan reaches a negative number we need to transfer less than 10%
      if (loan < 0) {
        let positive = Math.abs(loan);
        bank = bank + positive;
        document.getElementById("workBank").innerHTML = bank;
        loan = 0;
        document.getElementById("loan").innerHTML = loan;
      }
      //No loan full income goes to bank
    } else if (loan == 0) {
      document.getElementById("workBank").innerHTML = bank;
      money = 0;
      document.getElementById("workPay").innerHTML = money;
    }
  }
  //Button stays visable until we have paid the loan in full
  if (loan == 0) {
    document.getElementById("payLoanBtn").style.visibility = "hidden";
  }
};

//Function to get the Loan promp't
function getLoanFunction() {
  document.getElementById("myForm").style.display = "block";
}

//Function to pay the loan
payLoanFunction = () => {
  if (loan > 0 && money > 0) {
    loan = loan - money;
    //checks if number is negative, then we convert it to positive and move it to balance
    if (loan < 0) {
      let positive = Math.abs(loan);
      bank = bank + positive;
      document.getElementById("workBank").innerHTML = bank;
      loan = 0;
      money = 0;
      document.getElementById("loan").innerHTML = loan;
      document.getElementById("workPay").innerHTML = money;
    } else {
      document.getElementById("loan").innerHTML = loan;
      document.getElementById("workPay").innerHTML = money;
      money = 0;
      document.getElementById("workPay").innerHTML = money;
    }
  }
  //Button stays visable until we have paid the loan in full
  if (loan == 0) {
    document.getElementById("payLoanBtn").style.visibility = "hidden";
  }
};

closeForm = () => {
  document.getElementById("myForm").style.display = "none";
};

//Function to get a loan from the bank
loanFunction = () => {
  let loanInput = parseInt(document.getElementById("loanId").value);

  if (loanInput <= bank * 2 && loan == 0) {
    loan = loanInput;
    bank = bank + loan;
    document.getElementById("loan").innerHTML = loan;
    document.getElementById("workBank").innerHTML = bank;
    document.getElementById("myForm").style.display = "none";
    //make the payloan button visable
    if (loan != 0) {
      document.getElementById("payLoanBtn").style.visibility = "visible";
    }
  } else {
    alert("Can't grant loan. Either pay your loan or input a lower amount");
  }
};

/*
API part 
https://noroff-komputer-store-api.herokuapp.com/"
*/

//HTML elements
const selectedComputerPriceElement = document.getElementById(
  "selectedComputerPrice"
);
const selectedComputerDescElement = document.getElementById(
  "selectedComputerDesc"
);
const imagesElement = document.getElementById("image1");
const selectedComputerElement = document.getElementById("selectedComputer");
const specsElement = document.getElementById("specs");
const selectDropComputer = document.getElementById("computerID");
let = computers = [];

//Fetch information about computers
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => response.json())
  .then((data) => (computers = data))
  .then((computers) => addComputersToMenu(computers));

const addComputersToMenu = (computers) => {
  computers.forEach((comp) => addComputerToMenu(comp));
};

const addComputerToMenu = (computer) => {
  const computerElement = document.createElement("option");
  computerElement.value = computer.id;
  computerElement.appendChild(document.createTextNode(computer.title));
  selectDropComputer.appendChild(computerElement);
};

const handleComputerMenuChange = (e) => {
  const selectComputer = computers[e.target.selectedIndex];
  selectedComputerElement.innerHTML = selectComputer.title;
  selectedComputerDescElement.innerHTML = selectComputer.description;
  selectedComputerPriceElement.innerHTML = selectComputer.price;
  imagesElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + selectComputer.image;
  specsElement.innerHTML = selectComputer.specs;
};

selectDropComputer.addEventListener("change", handleComputerMenuChange);

buyLaptop = () => {
  if (bank >= selectedComputerPriceElement.innerHTML) {
    bank = bank - selectedComputerPriceElement.innerHTML;
    console.log(selectedComputerPriceElement);
    document.getElementById("workBank").innerHTML = bank;
    alert("Congratulations on your new computer!🎉");
  } else {
    alert("You can't afford this computer.⛔");
  }
};
